.. _TensorFlow:

==================================
TensorFlow
==================================

Google has released an open-source version of its machine-learning software named Tensorflow, which can allow for efficient backpropagation of neural networks and utilization of GPUs for extra speed.


Dependencies
---------------------------------

This package requires google's tensorflow; please see tensorflow's website for instructions for installation on your system. You should be able to run.


Example
---------------------------------
XXX

Known issues
---------------------------------
XXX

About
---------------------------------

This module was contributed by Zachary Ulissi (Department of Chemical Engineering, Stanford University, zulissi@gmail.com) with help, testing, and discussions from Andrew Doyle (Stanford) and the Amp development team.

